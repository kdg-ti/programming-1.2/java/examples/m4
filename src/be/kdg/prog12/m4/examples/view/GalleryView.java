package be.kdg.prog12.m4.examples.view;

import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class GalleryView extends BorderPane {
  private static final double BUTTON_WIDTH = 150.0;
  private final Accordion accordion = new Accordion();


  public GalleryView() {
    initialiseNodes();
    layoutNodes();
  }



  private void initialiseNodes() {
  }

  private void layoutNodes() {
    setMinWidth(BUTTON_WIDTH+50);
    setMinHeight(500);
    setCenter(accordion);
  }



  public Button addDemoButton(String name) {
    Button button  = new Button(name);
    ((Pane)accordion
        .getPanes()
        .get(accordion.getPanes().size()-1)
        .getContent()).getChildren().add(button);
    button.setMinWidth(BUTTON_WIDTH);
    return button;
  }

  public void startDemoSection(String key) {
    VBox sectionPane= new VBox();
    sectionPane.setPadding(new Insets(20.0, 0.0, 20.0, 0.0));
    sectionPane.setSpacing(10.0);
    sectionPane.setAlignment(Pos.TOP_CENTER);
    TitledPane titledPane = new TitledPane(key, sectionPane);
    accordion.getPanes().add(titledPane);
    if(accordion.getPanes().size() == 1) {
      accordion.setExpandedPane(titledPane);
    }
  }
}
