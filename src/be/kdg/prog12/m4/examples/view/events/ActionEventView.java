package be.kdg.prog12.m4.examples.view.events;

import javafx.geometry.Insets;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;

public class ActionEventView extends BorderPane {
    public ActionEventView() {
      ToggleButton toggleButton = new ToggleButton("On / Off");
      toggleButton.setOnAction(event ->
          System.out.println(toggleButton.isSelected() ? "On" : "Off")
      );

     setCenter(toggleButton);
      BorderPane.setMargin(toggleButton, new Insets(30.0));
    }
}
