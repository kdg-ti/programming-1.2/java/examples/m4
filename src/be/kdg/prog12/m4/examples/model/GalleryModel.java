package be.kdg.prog12.m4.examples.model;

import be.kdg.prog12.m4.examples.view.controls.*;
import be.kdg.prog12.m4.examples.view.events.*;
import be.kdg.prog12.m4.examples.view.pane.*;
import java.util.*;
import javafx.scene.layout.Pane;

public class GalleryModel {

  // linkehashmap preservers order
  private final Map<String,List<Pane>> demoViews = new LinkedHashMap<>();

  // add your demos here
  public GalleryModel() {
    demoViews.put("Controls",List.of(
        new LabelView1(),
        new LabelView2(),
        new ImageViewView(),
        new ButtonView(),
        new CheckBoxView(),
        new TextFieldView(),
        new ComboBoxView(),
        new MenuBarView()
    ));
    demoViews.put("Events",List.of(
        new ActionEventView(),
        new MouseEventView(),
        new KeyEventView()
    ));
    // Layout views
    demoViews.put("Layout Panes",List.of(
        new BorderPaneView(),
        new VBoxView(),
        new VBoxView2(),
        new VBoxView3(),
        new HBoxView(),
        new GridPaneView1(),
        new GridPaneView2(),
        new GridPaneView3(),
        new GridPaneView4(),
        new GridPaneView5()
    ));
  }

  public  Map<String,List<Pane>> getDemoViews() {
    return demoViews;
  }
}
