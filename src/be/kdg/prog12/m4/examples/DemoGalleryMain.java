package be.kdg.prog12.m4.examples;

import be.kdg.prog12.m4.examples.model.GalleryModel;
import be.kdg.prog12.m4.examples.view.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class DemoGalleryMain extends Application {
	@Override
	public void start(Stage stage) {
		GalleryModel model = new GalleryModel();
		GalleryView view = new GalleryView();
		Scene scene = new Scene(view);
		stage.setScene(scene);
		stage.setTitle("Examples M4");
		new GalleryPresenter(model, view);
		new StartPresenter(model, view);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
