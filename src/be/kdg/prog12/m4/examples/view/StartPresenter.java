package be.kdg.prog12.m4.examples.view;

import be.kdg.prog12.m4.examples.model.GalleryModel;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.WindowEvent;

import java.util.Optional;

import static javafx.scene.control.Alert.AlertType.CONFIRMATION;

public class StartPresenter {
	GalleryModel model;
	GalleryView view;

	public StartPresenter(GalleryModel model, GalleryView view) {
		this.model = model;
		this.view = view;
		addEventHandlers();
		updateView();
	}

	private void updateView() {
	}

	private void addEventHandlers() {
		view.getScene().getWindow().setOnCloseRequest(event -> {
				final Alert sure = new Alert(CONFIRMATION);
				sure.setHeaderText("Are you sure?");
				sure.setContentText("Are you sure you want to exit?");

				Optional<ButtonType> choice = sure.showAndWait();
				if (choice.isPresent() && choice.get().getText().equalsIgnoreCase("CANCEL")) {
					event.consume();
				}// end if
			}// end lambda
		);// end call setOnCloseRequest

		// using inner class
		//  stage.setOnCloseRequest(new ConfirmCloseHandler());

	}// end addEventHandlers

	private class ConfirmCloseHandler implements EventHandler<WindowEvent> {
		@Override
		public void handle(WindowEvent event) {
			final Alert sure = new Alert(CONFIRMATION);
			sure.setHeaderText("Are you sure?");
			sure.setContentText("Are you sure you want to exit?");
			Optional<ButtonType> choice = sure.showAndWait();
			if (choice.isPresent() && choice.get().getText().equalsIgnoreCase("CANCEL")) {
				event.consume();
			}
		}
	}
}
