package be.kdg.prog12.m4.examples.view.events;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;

public class MouseEventView extends BorderPane {
    public MouseEventView() {
      setTop(new Label("Move the mouse over the pane to see the coordinates."));
     setMinHeight(300);
     setPadding(new Insets(10));
      setOnMouseMoved(
          event ->
              System.out.printf("X: %3.0f, Y: %3.0f%n",
                  event.getX(), event.getY())
      );

    }

    /* using an inner class instead of a lambda
    class PrintMovesEventHandler implements EventHandler<MouseEvent>{
      @Override
      public void handle(MouseEvent event) {
        System.out.printf("X: %3.0f,Y: %3.0f%n",
          event.getX(),
          event.getY());
      }
    }
     */
}
