package be.kdg.prog12.m4.examples.view.events;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class KeyEventView extends BorderPane {
    public KeyEventView() {
      setTop(new Label("Typing will log the vowels in the console"));
      setPadding(new Insets(10));

      TextField textField = new TextField();

      textField.setOnKeyTyped(event -> {
        if ("aeiou".contains(event.getCharacter())) {
          System.out.println(event.getCharacter());
        }
      });

      setCenter(textField);
      BorderPane.setMargin(textField, new Insets(30.0));
    }
}
