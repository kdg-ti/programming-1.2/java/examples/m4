package be.kdg.prog12.m4.examples.view;


import be.kdg.prog12.m4.examples.model.GalleryModel;
import java.util.List;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.*;

public class GalleryPresenter {


  private final GalleryView view;
  private final GalleryModel model;

  public GalleryPresenter(GalleryModel model, GalleryView view) {
    this.model = model;
    this.view = view;
    addEventHandlers();
  }

  private void addEventHandlers() {
    Stage demoStage = new Stage();
    demoStage.initOwner(view.getScene().getWindow());
    demoStage.initModality(Modality.APPLICATION_MODAL);
	  for (Map.Entry<String,List<Pane>> demoSection : model.getDemoViews().entrySet()) {
      view.startDemoSection(demoSection.getKey());
      List<Pane> topicViews = demoSection.getValue();
      for (Pane demoView : topicViews) {
        String demoName = demoView.getClass().getSimpleName();
        view.addDemoButton(demoName)
            .setOnAction(new DemoButtonEventHandler(demoStage, new Scene(demoView), demoName));
      }
    }
  }

  private static class DemoButtonEventHandler implements EventHandler<ActionEvent> {
    private final Stage demoStage;
    private final Scene demoScene;
    private final String demoName;

    public DemoButtonEventHandler(Stage demoStage, Scene demoScene, String demoName) {
      this.demoStage = demoStage;
      this.demoScene = demoScene;
      this.demoName = demoName;
    }

    @Override
    public void handle(ActionEvent event) {
      Button button = (Button) event.getTarget();
      Window window =  button.getScene().getWindow();
      demoStage.setScene(demoScene);
      demoStage.setTitle(demoName);
      demoStage.setX(window.getX() + button.getLayoutX() + 100);
      demoStage.setY(window.getY() + button.getLayoutY() + 60);
      demoStage.showAndWait();
    }
  }
}
